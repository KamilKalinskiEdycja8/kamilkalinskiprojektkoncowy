package pl.lodz.p.it.spjava.dm.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.dm.model.Konto;
import pl.lodz.p.it.spjava.dm.model.Produkt;
import pl.lodz.p.it.spjava.dm.model.Zatwierdzonezlecenie;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-03T16:13:39")
@StaticMetamodel(Zlozonezlecenie.class)
public class Zlozonezlecenie_ { 

    public static volatile SingularAttribute<Zlozonezlecenie, Konto> konto;
    public static volatile ListAttribute<Zlozonezlecenie, Zatwierdzonezlecenie> zatwierdzonezlecenieList;
    public static volatile SingularAttribute<Zlozonezlecenie, Long> ilosc;
    public static volatile SingularAttribute<Zlozonezlecenie, Produkt> produkt;
    public static volatile SingularAttribute<Zlozonezlecenie, Long> id;
    public static volatile SingularAttribute<Zlozonezlecenie, Long> wartosc;

}