package pl.lodz.p.it.spjava.dm.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.dm.model.Konto;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-03T16:13:39")
@StaticMetamodel(Daneklient.class)
public class Daneklient_ { 

    public static volatile SingularAttribute<Daneklient, Konto> konto;
    public static volatile SingularAttribute<Daneklient, String> nip;
    public static volatile SingularAttribute<Daneklient, Long> id;

}