package pl.lodz.p.it.spjava.dm.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.dm.model.Konto;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-03T16:13:39")
@StaticMetamodel(Danepersonalne.class)
public class Danepersonalne_ { 

    public static volatile SingularAttribute<Danepersonalne, String> imie;
    public static volatile SingularAttribute<Danepersonalne, Konto> konto;
    public static volatile SingularAttribute<Danepersonalne, String> nazwisko;
    public static volatile SingularAttribute<Danepersonalne, String> telefon;
    public static volatile SingularAttribute<Danepersonalne, Long> id;
    public static volatile SingularAttribute<Danepersonalne, String> email;

}