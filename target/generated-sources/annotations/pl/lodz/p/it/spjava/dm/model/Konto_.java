package pl.lodz.p.it.spjava.dm.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.dm.model.Daneadministrator;
import pl.lodz.p.it.spjava.dm.model.Danebroker;
import pl.lodz.p.it.spjava.dm.model.Daneklient;
import pl.lodz.p.it.spjava.dm.model.Danepersonalne;
import pl.lodz.p.it.spjava.dm.model.Zatwierdzonezlecenie;
import pl.lodz.p.it.spjava.dm.model.Zlozonezlecenie;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-03T16:13:39")
@StaticMetamodel(Konto.class)
public class Konto_ { 

    public static volatile ListAttribute<Konto, Daneklient> daneklientList;
    public static volatile ListAttribute<Konto, Danebroker> danebrokerList;
    public static volatile ListAttribute<Konto, Daneadministrator> daneadministratorList;
    public static volatile SingularAttribute<Konto, Danebroker> danebroker;
    public static volatile SingularAttribute<Konto, String> typ;
    public static volatile SingularAttribute<Konto, String> login;
    public static volatile ListAttribute<Konto, Danepersonalne> danepersonalneList;
    public static volatile SingularAttribute<Konto, Short> aktywne;
    public static volatile ListAttribute<Konto, Zatwierdzonezlecenie> zatwierdzonezlecenieList;
    public static volatile SingularAttribute<Konto, Short> potwierdzenie;
    public static volatile SingularAttribute<Konto, String> haslo;
    public static volatile SingularAttribute<Konto, Danepersonalne> danepersonalne;
    public static volatile SingularAttribute<Konto, Long> id;
    public static volatile ListAttribute<Konto, Zlozonezlecenie> zlozonezlecenieList;

}