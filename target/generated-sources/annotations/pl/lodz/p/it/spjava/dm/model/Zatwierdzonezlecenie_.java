package pl.lodz.p.it.spjava.dm.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.dm.model.Danebroker;
import pl.lodz.p.it.spjava.dm.model.Konto;
import pl.lodz.p.it.spjava.dm.model.Zlozonezlecenie;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-03T16:13:39")
@StaticMetamodel(Zatwierdzonezlecenie.class)
public class Zatwierdzonezlecenie_ { 

    public static volatile SingularAttribute<Zatwierdzonezlecenie, Zlozonezlecenie> zlozonezlecenie;
    public static volatile SingularAttribute<Zatwierdzonezlecenie, Danebroker> danebroker;
    public static volatile SingularAttribute<Zatwierdzonezlecenie, Long> id;
    public static volatile SingularAttribute<Zatwierdzonezlecenie, Short> zatwierdzone;
    public static volatile ListAttribute<Zatwierdzonezlecenie, Konto> kontoList;

}