package pl.lodz.p.it.spjava.dm.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.dm.model.Konto;
import pl.lodz.p.it.spjava.dm.model.Zatwierdzonezlecenie;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-03T16:13:39")
@StaticMetamodel(Danebroker.class)
public class Danebroker_ { 

    public static volatile SingularAttribute<Danebroker, Integer> aktywne;
    public static volatile SingularAttribute<Danebroker, String> imie;
    public static volatile SingularAttribute<Danebroker, Konto> konto;
    public static volatile ListAttribute<Danebroker, Zatwierdzonezlecenie> zatwierdzonezlecenieList;
    public static volatile SingularAttribute<Danebroker, String> telefonsupport;
    public static volatile SingularAttribute<Danebroker, String> nazwisko;
    public static volatile SingularAttribute<Danebroker, String> telefon;
    public static volatile SingularAttribute<Danebroker, String> haslo;
    public static volatile SingularAttribute<Danebroker, Long> id;
    public static volatile SingularAttribute<Danebroker, String> login;
    public static volatile SingularAttribute<Danebroker, String> email;

}