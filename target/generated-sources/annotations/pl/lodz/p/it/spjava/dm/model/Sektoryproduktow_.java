package pl.lodz.p.it.spjava.dm.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.dm.model.Produkt;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-03T16:13:39")
@StaticMetamodel(Sektoryproduktow.class)
public class Sektoryproduktow_ { 

    public static volatile SingularAttribute<Sektoryproduktow, Long> id;
    public static volatile SingularAttribute<Sektoryproduktow, String> nazwa;
    public static volatile SingularAttribute<Sektoryproduktow, String> opis;
    public static volatile ListAttribute<Sektoryproduktow, Produkt> produktList;

}