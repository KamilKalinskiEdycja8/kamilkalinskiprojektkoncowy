package pl.lodz.p.it.spjava.dm.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.dm.model.Sektoryproduktow;
import pl.lodz.p.it.spjava.dm.model.Zlozonezlecenie;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-03T16:13:39")
@StaticMetamodel(Produkt.class)
public class Produkt_ { 

    public static volatile ListAttribute<Produkt, Sektoryproduktow> sektoryproduktowList;
    public static volatile SingularAttribute<Produkt, String> nazwapelna;
    public static volatile SingularAttribute<Produkt, Long> stan;
    public static volatile SingularAttribute<Produkt, Double> kurs;
    public static volatile SingularAttribute<Produkt, String> skrot;
    public static volatile SingularAttribute<Produkt, Long> id;
    public static volatile ListAttribute<Produkt, Zlozonezlecenie> zlozonezlecenieList;

}