/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.ejb.facades;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.lodz.p.it.spjava.dm.model.Sektoryproduktow;

/**
 *
 * @author java
 */
@Stateless
public class SektoryproduktowFacade extends AbstractFacade<Sektoryproduktow> {

    @PersistenceContext(unitName = "Kamil_Kalinski_edycja_JEE8_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SektoryproduktowFacade() {
        super(Sektoryproduktow.class);
    }
    
}
