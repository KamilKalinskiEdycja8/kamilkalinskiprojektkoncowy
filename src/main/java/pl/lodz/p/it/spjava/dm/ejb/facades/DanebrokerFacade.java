/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.ejb.facades;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.lodz.p.it.spjava.dm.model.Danebroker;

/**
 *
 * @author java
 */
@Stateless
public class DanebrokerFacade extends AbstractFacade<Danebroker> {

    @PersistenceContext(unitName = "Kamil_Kalinski_edycja_JEE8_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DanebrokerFacade() {
        super(Danebroker.class);
    }
    
}
