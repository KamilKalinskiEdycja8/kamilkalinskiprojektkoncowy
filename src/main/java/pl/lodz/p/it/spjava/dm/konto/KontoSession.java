
package pl.lodz.p.it.spjava.dm.konto;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.inject.Inject;
import pl.lodz.p.it.spjava.dm.ejb.endpoints.KontoEndpoint;
import pl.lodz.p.it.spjava.dm.model.Danebroker;

@Named(value = "kontoSession")
@SessionScoped
public class KontoSession implements Serializable {

    public KontoSession() {
    }
    
    @Inject
    private KontoEndpoint kontoEndpoint;
    
    private Danebroker nowyBroker;
    
    public String utworzBrokera(Danebroker nowyBroker){
//    this.nowyBroker = nowyBroker;
    kontoEndpoint.utworzBrokera(nowyBroker);
//    this.nowyBroker=null;
    return "potwierdzenieProdukt";
    }
}
