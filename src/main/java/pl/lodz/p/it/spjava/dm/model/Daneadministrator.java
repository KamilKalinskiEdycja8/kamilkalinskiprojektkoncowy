/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author java
 */
@Entity
@Table(name = "DANEADMINISTRATOR")
@NamedQueries({
    @NamedQuery(name = "Daneadministrator.findAll", query = "SELECT d FROM Daneadministrator d")
    , @NamedQuery(name = "Daneadministrator.findById", query = "SELECT d FROM Daneadministrator d WHERE d.id = :id")
    , @NamedQuery(name = "Daneadministrator.findByTelefonalarm", query = "SELECT d FROM Daneadministrator d WHERE d.telefonalarm = :telefonalarm")})
public class Daneadministrator implements Serializable {

    private static final long serialVersionUID = 1L;
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "TELEFONALARM")
    private String telefonalarm;
    @JoinColumn(name = "ID_KONTO", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Konto konto;

    public Daneadministrator() {
    }

    public Daneadministrator(Long id) {
        this.id = id;
    }

    public Daneadministrator(Long id, String telefonalarm) {
        this.id = id;
        this.telefonalarm = telefonalarm;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTelefonalarm() {
        return telefonalarm;
    }

    public void setTelefonalarm(String telefonalarm) {
        this.telefonalarm = telefonalarm;
    }

    public Konto getKonto() {
        return konto;
    }

    public void setKonto(Konto konto) {
        this.konto = konto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Daneadministrator)) {
            return false;
        }
        Daneadministrator other = (Daneadministrator) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.dm.model.Daneadministrator[ id=" + id + " ]";
    }
    
}
