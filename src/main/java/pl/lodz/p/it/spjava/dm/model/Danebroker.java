/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author java
 */
@Entity
@Table(name = "Danebroker")
@SecondaryTables({@SecondaryTable(name="Konto", pkJoinColumns = {@PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")}),
    @SecondaryTable(name="Danepersonalne")})
@NamedQueries({
    @NamedQuery(name = "Danebroker.findAll", query = "SELECT d FROM Danebroker d")
    , @NamedQuery(name = "Danebroker.findById", query = "SELECT d FROM Danebroker d WHERE d.id = :id")
    , @NamedQuery(name = "Danebroker.findByTelefonsupport", query = "SELECT d FROM Danebroker d WHERE d.telefonsupport = :telefonsupport")})
public class Danebroker implements Serializable {

    private static final long serialVersionUID = 1L;
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "TELEFONSUPPORT")
    private String telefonsupport;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "danebroker")
    private List<Zatwierdzonezlecenie> zatwierdzonezlecenieList;
    @OneToOne
//    @JoinColumn(name = "KONTO_ID", referencedColumnName = "ID")
    private Konto konto;
    
    @Column(name = "LOGIN", table = "Konto")
    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
        
    @Column(name = "HASLO", table = "Konto")
    private String haslo;

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    @Column(name="AKTYWNE", table="Konto")
    private int aktywne;

    public int getAktywne() {
        return aktywne;
    }

    public void setAktywne(int aktywne) {
        this.aktywne = aktywne;
    }
    @Column(name = "IMIE", table = "Danepersonalne")
    private String imie;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }
    @Column(name = "NAZWISKO", table = "Danepersonalne")
    private String nazwisko;

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }
    @Column(name = "EMAIL", table = "Danepersonalne")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Column(name = "TELEFON", table = "Danepersonalne")
    private String telefon;

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    
    
    
    public Danebroker() {
    }

    public Danebroker(Long id) {
        this.id = id;
    }

    public Danebroker(Long id, String telefonsupport) {
        this.id = id;
        this.telefonsupport = telefonsupport;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTelefonsupport() {
        return telefonsupport;
    }

    public void setTelefonsupport(String telefonsupport) {
        this.telefonsupport = telefonsupport;
    }

    public List<Zatwierdzonezlecenie> getZatwierdzonezlecenieList() {
        return zatwierdzonezlecenieList;
    }

    public void setZatwierdzonezlecenieList(List<Zatwierdzonezlecenie> zatwierdzonezlecenieList) {
        this.zatwierdzonezlecenieList = zatwierdzonezlecenieList;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Danebroker)) {
            return false;
        }
        Danebroker other = (Danebroker) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.dm.model.Danebroker[ id=" + id + " ]";
    }
    
}
