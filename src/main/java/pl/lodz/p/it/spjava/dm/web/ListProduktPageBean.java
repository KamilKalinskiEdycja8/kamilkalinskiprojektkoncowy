/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.web;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.DataModel;
import javax.inject.Inject;
import pl.lodz.p.it.spjava.dm.model.Produkt;
import pl.lodz.p.it.spjava.dm.produkt.ProduktSession;

/**
 *
 * @author java
 */
@Named(value = "listProduktPageBean")
@RequestScoped
public class ListProduktPageBean {
    
    @Inject
    private ProduktSession produktSession;
    
    private DataModel<Produkt> dataModelProdukt;
    private DataModel<Produkt> dataModelOneProdukt;
    
    private String produktSkrot;
    private static String wybranyProdukt;

    public String getWybranyProdukt() {
        return wybranyProdukt;
    }

    public void setWybranyProdukt(String wybranyProdukt) {
        this.wybranyProdukt = wybranyProdukt;
    }
    
    public ListProduktPageBean() {
    }
    
    public DataModel<Produkt> getProduktList() {
        return produktSession.getProduktList();
    }
    
    public String getProduktSkrot() {
        return produktSkrot;
    }
    
    public DataModel<Produkt> getOneProduktList(){
        return produktSession.getOneProduktList(wybranyProdukt);
    }
    
    public String szczegolyProduktu(){
        return "produktDetailsPage";
    }

}

