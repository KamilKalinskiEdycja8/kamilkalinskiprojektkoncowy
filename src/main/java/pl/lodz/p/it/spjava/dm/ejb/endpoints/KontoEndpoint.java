
package pl.lodz.p.it.spjava.dm.ejb.endpoints;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import pl.lodz.p.it.spjava.dm.ejb.facades.DanebrokerFacade;
import pl.lodz.p.it.spjava.dm.model.Danebroker;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class KontoEndpoint {

    @Inject
    private DanebrokerFacade danebrokerFacade;
    
    public void utworzBrokera(Danebroker nowyBroker){
    nowyBroker.setAktywne(1);
    danebrokerFacade.create(nowyBroker);
    }
}
