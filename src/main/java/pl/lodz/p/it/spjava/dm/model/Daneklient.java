/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author java
 */
@Entity
@Table(name = "DANEKLIENT")
@NamedQueries({
    @NamedQuery(name = "Daneklient.findAll", query = "SELECT d FROM Daneklient d")
    , @NamedQuery(name = "Daneklient.findById", query = "SELECT d FROM Daneklient d WHERE d.id = :id")
    , @NamedQuery(name = "Daneklient.findByNip", query = "SELECT d FROM Daneklient d WHERE d.nip = :nip")})
public class Daneklient implements Serializable {

    private static final long serialVersionUID = 1L;
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "NIP")
    private String nip;
    @JoinColumn(name = "ID_KONTO", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Konto konto;

    public Daneklient() {
    }

    public Daneklient(Long id) {
        this.id = id;
    }

    public Daneklient(Long id, String nip) {
        this.id = id;
        this.nip = nip;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public Konto getKonto() {
        return konto;
    }

    public void setKonto(Konto konto) {
        this.konto = konto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Daneklient)) {
            return false;
        }
        Daneklient other = (Daneklient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.dm.model.Daneklient[ id=" + id + " ]";
    }
    
}
