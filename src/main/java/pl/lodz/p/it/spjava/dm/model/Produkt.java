/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author java
 */
@Entity
@Table(name = "PRODUKT")
@NamedQueries({
    @NamedQuery(name = "Produkt.findAll", query = "SELECT p FROM Produkt p")
    , @NamedQuery(name = "Produkt.findById", query = "SELECT p FROM Produkt p WHERE p.id = :id")
    , @NamedQuery(name = "Produkt.findByKurs", query = "SELECT p FROM Produkt p WHERE p.kurs = :kurs")
    , @NamedQuery(name = "Produkt.findByNazwapelna", query = "SELECT p FROM Produkt p WHERE p.nazwapelna = :nazwapelna")
    , @NamedQuery(name = "Produkt.findBySkrot", query = "SELECT p FROM Produkt p WHERE p.skrot = :skrot")
    , @NamedQuery(name = "Produkt.findByStan", query = "SELECT p FROM Produkt p WHERE p.stan = :stan")})
public class Produkt implements Serializable {

    private static final long serialVersionUID = 1L;
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "KURS")
    private Double kurs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "NAZWAPELNA")
    private String nazwapelna;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "SKROT")
    private String skrot;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STAN")
    private long stan;
    @JoinTable(name = "PRZYPISANIESEKTORY", joinColumns = {
        @JoinColumn(name = "ID_PRODUKT", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "ID_SEKTORYPRODUKTOW", referencedColumnName = "ID")})
    @ManyToMany
    private List<Sektoryproduktow> sektoryproduktowList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "produkt")
    private List<Zlozonezlecenie> zlozonezlecenieList;

    public Produkt() {
    }

    public Produkt(Long id) {
        this.id = id;
    }

    public Produkt(Long id, String nazwapelna, String skrot, long stan) {
        this.id = id;
        this.nazwapelna = nazwapelna;
        this.skrot = skrot;
        this.stan = stan;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getKurs() {
        return kurs;
    }

    public void setKurs(Double kurs) {
        this.kurs = kurs;
    }

    public String getNazwapelna() {
        return nazwapelna;
    }

    public void setNazwapelna(String nazwapelna) {
        this.nazwapelna = nazwapelna;
    }

    public String getSkrot() {
        return skrot;
    }

    public void setSkrot(String skrot) {
        this.skrot = skrot;
    }

    public long getStan() {
        return stan;
    }

    public void setStan(long stan) {
        this.stan = stan;
    }

    public List<Sektoryproduktow> getSektoryproduktowList() {
        return sektoryproduktowList;
    }

    public void setSektoryproduktowList(List<Sektoryproduktow> sektoryproduktowList) {
        this.sektoryproduktowList = sektoryproduktowList;
    }

    public List<Zlozonezlecenie> getZlozonezlecenieList() {
        return zlozonezlecenieList;
    }

    public void setZlozonezlecenieList(List<Zlozonezlecenie> zlozonezlecenieList) {
        this.zlozonezlecenieList = zlozonezlecenieList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produkt)) {
            return false;
        }
        Produkt other = (Produkt) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.dm.model.Produkt[ id=" + id + " ]";
    }
    
}
