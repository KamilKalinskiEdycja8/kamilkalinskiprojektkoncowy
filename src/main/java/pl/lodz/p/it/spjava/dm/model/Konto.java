/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author java
 */
@Entity
@Table(name = "Konto")
@NamedQueries({
    @NamedQuery(name = "Konto.findAll", query = "SELECT k FROM Konto k")
    , @NamedQuery(name = "Konto.findById", query = "SELECT k FROM Konto k WHERE k.id = :id")
    , @NamedQuery(name = "Konto.findByTyp", query = "SELECT k FROM Konto k WHERE k.typ = :typ")
    , @NamedQuery(name = "Konto.findByAktywne", query = "SELECT k FROM Konto k WHERE k.aktywne = :aktywne")
    , @NamedQuery(name = "Konto.findByLogin", query = "SELECT k FROM Konto k WHERE k.login = :login")
    , @NamedQuery(name = "Konto.findByHaslo", query = "SELECT k FROM Konto k WHERE k.haslo = :haslo")
    , @NamedQuery(name = "Konto.findByPotwierdzenie", query = "SELECT k FROM Konto k WHERE k.potwierdzenie = :potwierdzenie")})
public class Konto implements Serializable {

    private static final long serialVersionUID = 1L;
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 15)
    @Column(name = "TYP")
    private String typ;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AKTYWNE")
    private short aktywne;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "LOGIN")
    private String login;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "HASLO")
    private String haslo;
    @Column(name = "POTWIERDZENIE")
    private Short potwierdzenie;
    @JoinTable(name = "PORTFEL", joinColumns = {
        @JoinColumn(name = "KONTO_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "ZATWIERDZONEZLECENIE_ID", referencedColumnName = "ID")})
    @ManyToMany
    private List<Zatwierdzonezlecenie> zatwierdzonezlecenieList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "konto")
    private List<Zlozonezlecenie> zlozonezlecenieList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "konto")
    private List<Danebroker> danebrokerList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "konto")
    private List<Danepersonalne> danepersonalneList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "konto")
    private List<Daneadministrator> daneadministratorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "konto")
    private List<Daneklient> daneklientList;
    @OneToOne(mappedBy = "konto")
    private Danebroker danebroker;
    @OneToOne(mappedBy = "konto")
    private Danepersonalne danepersonalne;

    public Konto() {
    }

    public Konto(Long id) {
        this.id = id;
    }

    public Konto(Long id, short aktywne, String login, String haslo) {
        this.id = id;
        this.aktywne = aktywne;
        this.login = login;
        this.haslo = haslo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public short getAktywne() {
        return aktywne;
    }

    public void setAktywne(short aktywne) {
        this.aktywne = aktywne;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public Short getPotwierdzenie() {
        return potwierdzenie;
    }

    public void setPotwierdzenie(Short potwierdzenie) {
        this.potwierdzenie = potwierdzenie;
    }

    public List<Zatwierdzonezlecenie> getZatwierdzonezlecenieList() {
        return zatwierdzonezlecenieList;
    }

    public void setZatwierdzonezlecenieList(List<Zatwierdzonezlecenie> zatwierdzonezlecenieList) {
        this.zatwierdzonezlecenieList = zatwierdzonezlecenieList;
    }

    public List<Zlozonezlecenie> getZlozonezlecenieList() {
        return zlozonezlecenieList;
    }

    public void setZlozonezlecenieList(List<Zlozonezlecenie> zlozonezlecenieList) {
        this.zlozonezlecenieList = zlozonezlecenieList;
    }

    public List<Danebroker> getDanebrokerList() {
        return danebrokerList;
    }

    public void setDanebrokerList(List<Danebroker> danebrokerList) {
        this.danebrokerList = danebrokerList;
    }

    public List<Danepersonalne> getDanepersonalneList() {
        return danepersonalneList;
    }

    public void setDanepersonalneList(List<Danepersonalne> danepersonalneList) {
        this.danepersonalneList = danepersonalneList;
    }

    public List<Daneadministrator> getDaneadministratorList() {
        return daneadministratorList;
    }

    public void setDaneadministratorList(List<Daneadministrator> daneadministratorList) {
        this.daneadministratorList = daneadministratorList;
    }

    public List<Daneklient> getDaneklientList() {
        return daneklientList;
    }

    public void setDaneklientList(List<Daneklient> daneklientList) {
        this.daneklientList = daneklientList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Konto)) {
            return false;
        }
        Konto other = (Konto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.dm.model.Konto[ id=" + id + " ]";
    }
    
}
