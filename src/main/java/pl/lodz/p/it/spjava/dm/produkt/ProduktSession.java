/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.produkt;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import pl.lodz.p.it.spjava.dm.ejb.endpoints.ProduktEndpoint;
import pl.lodz.p.it.spjava.dm.ejb.facades.ProduktFacade;
import pl.lodz.p.it.spjava.dm.model.Produkt;

/**
 *
 * @author java
 */
@Named(value = "produktSession")
@SessionScoped
public class ProduktSession implements Serializable {
    
    @Inject
    private ProduktEndpoint produktEndpoint;
    
    private Produkt produkt;
    private Produkt produktUtworz;
    private DataModel<Produkt> dataModelOneProdukt;
    private DataModel<Produkt> dataModelProdukt;

    public Produkt getProdukt() {
        return produkt;
    }

    public void setProdukt(Produkt produkt) {
        this.produkt = produkt;
    }
        
    public ProduktSession() {
    }

    public String utworzProdukt(Produkt nowyProdukt){
        produktUtworz = nowyProdukt;
        produktEndpoint.utworzProdukt(produktUtworz);
        produktUtworz=null;
        return "potwierdzenieProdukt";
    }
    
    public DataModel<Produkt> getOneProduktList(String wybranyProdukt){
        return (dataModelOneProdukt = new ListDataModel<>(produktEndpoint.getOneProduktList(wybranyProdukt)));
    }
    
    public DataModel<Produkt> getProduktList() {
        return (dataModelProdukt = new ListDataModel<>(produktEndpoint.getProduktList()));
    }
    
}   

