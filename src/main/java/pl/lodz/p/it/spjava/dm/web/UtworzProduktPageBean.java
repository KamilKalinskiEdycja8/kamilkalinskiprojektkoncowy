/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.web;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import pl.lodz.p.it.spjava.dm.model.Produkt;
import pl.lodz.p.it.spjava.dm.produkt.ProduktSession;

/**
 *
 * @author java
 */
@Named(value = "utworzProduktPageBean")
@RequestScoped
public class UtworzProduktPageBean {

    public UtworzProduktPageBean() {
    }
    
    @Inject
    private ProduktSession produktSession;
    
    private Produkt nowyProdukt = new Produkt();

    public Produkt getNowyProdukt() {
        return nowyProdukt;
    }
    
    public String utworzProdukt(){    
        return produktSession.utworzProdukt(nowyProdukt);
    }
    
}

