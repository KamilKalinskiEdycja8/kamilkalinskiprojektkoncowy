/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author java
 */
/*
@Entity
@Table(name = "DANELOGOWANIE")
@NamedQueries({
    @NamedQuery(name = "Danelogowanie.findAll", query = "SELECT d FROM Danelogowanie d")
    , @NamedQuery(name = "Danelogowanie.findByLogin", query = "SELECT d FROM Danelogowanie d WHERE d.login = :login")
    , @NamedQuery(name = "Danelogowanie.findByHaslo", query = "SELECT d FROM Danelogowanie d WHERE d.haslo = :haslo")
    , @NamedQuery(name = "Danelogowanie.findByTyp", query = "SELECT d FROM Danelogowanie d WHERE d.typ = :typ")})
public class Danelogowanie implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "LOGIN")
    private String login;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "HASLO")
    private String haslo;
    @Size(max = 15)
    @Column(name = "TYP")
    private String typ;

    public Danelogowanie() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }
    
}
*/