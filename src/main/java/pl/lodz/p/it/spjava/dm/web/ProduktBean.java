/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.web;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;
import pl.lodz.p.it.spjava.dm.dto.ProduktDTO;
import pl.lodz.p.it.spjava.dm.ejb.endpoints.ProduktEndpoint;
import pl.lodz.p.it.spjava.dm.model.Produkt;
import pl.lodz.p.it.spjava.dm.produkt.ProduktSession;

/**
 *
 * @author java
 */
@Named(value = "produktBean")
@SessionScoped
public class ProduktBean implements Serializable {

    @EJB
    private ProduktEndpoint produktEndpoint;
    
    @Inject
    private ProduktSession produktSession;
    
    private Produkt produkt = new Produkt();

    public Produkt getProdukt() {
        return produkt;
    }
        
    private List<ProduktDTO> listProdukt;
    
    private ProduktDTO produktDTO;

    public ProduktDTO getProduktDTO() {
        return produktDTO;
    }

    public void setProduktDTO(ProduktDTO produktDTO) {
        this.produktDTO = produktDTO;
    }

    public List<ProduktDTO> getListProdukt() {
        return listProdukt;
    }
    
    public ProduktBean() {
    }
     
    @PostConstruct
    public void loadProdukty(){
        listProdukt = produktEndpoint.loadProduktList();
    }
    
    public void utowrzProdukt(){
          produktSession.utworzProdukt(produkt);
    }
    

    
}
