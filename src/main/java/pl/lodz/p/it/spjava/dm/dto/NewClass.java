package pl.lodz.p.it.spjava.dm.dto;


import javax.faces.bean.SessionScoped;

/*
*   przyciski:
*   - zaloguj,
*   - wyloguj,
*   - strona główna,
*
*   widok gość:
*   - zaloguj,
*   - lista produktów,
*   - zarejestruj klienta,
*   - zarejestruj brokera,
*
*   widoki klient:
*   - update swoje dane,
*   - lista/edycja portfela,
*   - złóż zlecenie,
*   - potwierdź zlecenie,
*   - historia zleceń,
*
*   widoki broker:
*   - potwierdz zlecenie klienta,
*   - lista/edycja/dodanie produktów,
* 
*   widoki admin:
*   - lista/edycja kont klientow,
*   - lista/edycja kont brokerów,
*   - potwierdz rejestracje konta,
*   - utworz konto kolejnego admina,
*   - update swoje dane,
*
 */

/**
 *
 * @author java
 */
public class NewClass {
    
    public static String zmienna;

    public String getZmienna() {
        return zmienna;
    }

    public void setZmienna(String zmienna) {
        this.zmienna = zmienna;
    }
}
