/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author java
 */
@Entity
@Table(name = "ZATWIERDZONEZLECENIE")
@NamedQueries({
    @NamedQuery(name = "Zatwierdzonezlecenie.findAll", query = "SELECT z FROM Zatwierdzonezlecenie z")
    , @NamedQuery(name = "Zatwierdzonezlecenie.findById", query = "SELECT z FROM Zatwierdzonezlecenie z WHERE z.id = :id")
    , @NamedQuery(name = "Zatwierdzonezlecenie.findByZatwierdzone", query = "SELECT z FROM Zatwierdzonezlecenie z WHERE z.zatwierdzone = :zatwierdzone")})
public class Zatwierdzonezlecenie implements Serializable {

    private static final long serialVersionUID = 1L;
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ZATWIERDZONE")
    private short zatwierdzone;
    @ManyToMany(mappedBy = "zatwierdzonezlecenieList")
    private List<Konto> kontoList;
    @JoinColumn(name = "ID_DANEBROKER", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Danebroker danebroker;
    @JoinColumn(name = "ID_ZLOZONEZLECENIE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Zlozonezlecenie zlozonezlecenie;

    public Zatwierdzonezlecenie() {
    }

    public Zatwierdzonezlecenie(Long id) {
        this.id = id;
    }

    public Zatwierdzonezlecenie(Long id, short zatwierdzone) {
        this.id = id;
        this.zatwierdzone = zatwierdzone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public short getZatwierdzone() {
        return zatwierdzone;
    }

    public void setZatwierdzone(short zatwierdzone) {
        this.zatwierdzone = zatwierdzone;
    }

    public List<Konto> getKontoList() {
        return kontoList;
    }

    public void setKontoList(List<Konto> kontoList) {
        this.kontoList = kontoList;
    }

    public Danebroker getDanebroker() {
        return danebroker;
    }

    public void setDanebroker(Danebroker danebroker) {
        this.danebroker = danebroker;
    }

    public Zlozonezlecenie getZlozonezlecenie() {
        return zlozonezlecenie;
    }

    public void setZlozonezlecenie(Zlozonezlecenie zlozonezlecenie) {
        this.zlozonezlecenie = zlozonezlecenie;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zatwierdzonezlecenie)) {
            return false;
        }
        Zatwierdzonezlecenie other = (Zatwierdzonezlecenie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.dm.model.Zatwierdzonezlecenie[ id=" + id + " ]";
    }
    
}
