
package pl.lodz.p.it.spjava.dm.web;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import pl.lodz.p.it.spjava.dm.konto.KontoSession;
import pl.lodz.p.it.spjava.dm.model.Danebroker;

@Named(value = "utworzBrokeraPageBean")
@RequestScoped
public class UtworzBrokeraPageBean {

    public UtworzBrokeraPageBean() {
    }
    
    @Inject
    private KontoSession kontoSession;
    
    private Danebroker nowyBroker = new Danebroker();

    public Danebroker getNowyBroker() {
        return nowyBroker;
    }
 
    private String hasloPowtorz = "";

    public String getHasloPowtorz() {
        return hasloPowtorz;
    }

    public void setHasloPowtorz(String hasloPowtorz) {
        this.hasloPowtorz = hasloPowtorz;
    }
    
    public String utworz() {
        return kontoSession.utworzBrokera(nowyBroker);
    }
}



