/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.dto;

/**
 *
 * @author java
 */
public class ProduktDTO {
    
    private Double kurs;
    private String skrot;
    private Long stan;
    private String nazwapelna;

    public ProduktDTO(Double kurs, String skrot, Long stan, String nazwapelna) {
        this.kurs = kurs;
        this.skrot = skrot;
        this.stan = stan;
        this.nazwapelna = nazwapelna;
    }

    public Long getStan() {
        return stan;
    }

    public void setStan(Long stan) {
        this.stan = stan;
    }

    public String getNazwapelna() {
        return nazwapelna;
    }

    public void setNazwapelna(String nazwapelna) {
        this.nazwapelna = nazwapelna;
    }
    

    public ProduktDTO() {
    }
    

    public Double getKurs() {
        return kurs;
    }

    public void setKurs(Double kurs) {
        this.kurs = kurs;
    }

    public String getSkrot() {
        return skrot;
    }

    public void setSkrot(String skrot) {
        this.skrot = skrot;
    }

    @Override
    public String toString() {
        return "Kurs=" + kurs + ", Skrot=" + skrot ;
    }
    
}
