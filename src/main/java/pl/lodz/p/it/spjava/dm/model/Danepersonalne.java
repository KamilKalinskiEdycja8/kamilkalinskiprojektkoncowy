/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author java
 */
@Entity
@Table(name = "Danepersonalne")
@NamedQueries({
    @NamedQuery(name = "Danepersonalne.findAll", query = "SELECT d FROM Danepersonalne d")
    , @NamedQuery(name = "Danepersonalne.findById", query = "SELECT d FROM Danepersonalne d WHERE d.id = :id")
    , @NamedQuery(name = "Danepersonalne.findByImie", query = "SELECT d FROM Danepersonalne d WHERE d.imie = :imie")
    , @NamedQuery(name = "Danepersonalne.findByNazwisko", query = "SELECT d FROM Danepersonalne d WHERE d.nazwisko = :nazwisko")
    , @NamedQuery(name = "Danepersonalne.findByEmail", query = "SELECT d FROM Danepersonalne d WHERE d.email = :email")
    , @NamedQuery(name = "Danepersonalne.findByTelefon", query = "SELECT d FROM Danepersonalne d WHERE d.telefon = :telefon")})

public class Danepersonalne implements Serializable {

    private static final long serialVersionUID = 1L;
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "IMIE")
    private String imie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NAZWISKO")
    private String nazwisko;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EMAIL")
    private String email;
    @Size(max = 12)
    @Column(name = "TELEFON")
    private String telefon;
    @OneToOne
    @JoinColumn(name = "KONTO_ID", referencedColumnName = "ID")
    private Konto konto;

    public Danepersonalne() {
    }

    public Danepersonalne(Long id) {
        this.id = id;
    }

    public Danepersonalne(Long id, String imie, String nazwisko, String email) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

//    public Konto getKonto() {
//        return konto;
//    }
//
//    public void setKonto(Konto konto) {
//        this.konto = konto;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Danepersonalne)) {
            return false;
        }
        Danepersonalne other = (Danepersonalne) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.dm.model.Danepersonalne[ id=" + id + " ]";
    }
    
}
