/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.ejb.facades;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import pl.lodz.p.it.spjava.dm.model.Produkt;

/**
 *
 * @author java
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class ProduktFacade extends AbstractFacade<Produkt> {

    @PersistenceContext(unitName = "Kamil_Kalinski_edycja_JEE8_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProduktFacade() {
        super(Produkt.class);
    }
    
    public List<Produkt> findProdukt(String wybranyProdukt){
        TypedQuery<Produkt> tq = em.createNamedQuery("Produkt.findBySkrot", Produkt.class);
        tq.setParameter("skrot", wybranyProdukt);
        return  tq.getResultList();
    }
    
}
