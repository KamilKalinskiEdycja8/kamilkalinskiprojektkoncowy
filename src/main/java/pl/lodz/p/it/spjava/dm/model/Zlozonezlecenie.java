/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author java
 */
@Entity
@Table(name = "ZLOZONEZLECENIE")
@NamedQueries({
    @NamedQuery(name = "Zlozonezlecenie.findAll", query = "SELECT z FROM Zlozonezlecenie z")
    , @NamedQuery(name = "Zlozonezlecenie.findById", query = "SELECT z FROM Zlozonezlecenie z WHERE z.id = :id")
    , @NamedQuery(name = "Zlozonezlecenie.findByWartosc", query = "SELECT z FROM Zlozonezlecenie z WHERE z.wartosc = :wartosc")
    , @NamedQuery(name = "Zlozonezlecenie.findByIlosc", query = "SELECT z FROM Zlozonezlecenie z WHERE z.ilosc = :ilosc")})
public class Zlozonezlecenie implements Serializable {

    private static final long serialVersionUID = 1L;
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WARTOSC")
    private long wartosc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ILOSC")
    private long ilosc;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "zlozonezlecenie")
    private List<Zatwierdzonezlecenie> zatwierdzonezlecenieList;
    @JoinColumn(name = "ID_KOTNO", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Konto konto;
    @JoinColumn(name = "ID_PRODUKT", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Produkt produkt;

    public Zlozonezlecenie() {
    }

    public Zlozonezlecenie(Long id) {
        this.id = id;
    }

    public Zlozonezlecenie(Long id, long wartosc, long ilosc) {
        this.id = id;
        this.wartosc = wartosc;
        this.ilosc = ilosc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getWartosc() {
        return wartosc;
    }

    public void setWartosc(long wartosc) {
        this.wartosc = wartosc;
    }

    public long getIlosc() {
        return ilosc;
    }

    public void setIlosc(long ilosc) {
        this.ilosc = ilosc;
    }

    public List<Zatwierdzonezlecenie> getZatwierdzonezlecenieList() {
        return zatwierdzonezlecenieList;
    }

    public void setZatwierdzonezlecenieList(List<Zatwierdzonezlecenie> zatwierdzonezlecenieList) {
        this.zatwierdzonezlecenieList = zatwierdzonezlecenieList;
    }

    public Konto getKonto() {
        return konto;
    }

    public void setKonto(Konto konto) {
        this.konto = konto;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public void setProdukt(Produkt produkt) {
        this.produkt = produkt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zlozonezlecenie)) {
            return false;
        }
        Zlozonezlecenie other = (Zlozonezlecenie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.dm.model.Zlozonezlecenie[ id=" + id + " ]";
    }
    
}
