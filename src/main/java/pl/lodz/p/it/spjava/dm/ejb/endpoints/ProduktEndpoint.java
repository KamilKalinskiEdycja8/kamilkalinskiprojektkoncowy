/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.ejb.endpoints;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import pl.lodz.p.it.spjava.dm.dto.ProduktDTO;
import pl.lodz.p.it.spjava.dm.ejb.facades.ProduktFacade;
import pl.lodz.p.it.spjava.dm.model.Produkt;

/**
 *
 * @author java
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class ProduktEndpoint {

    @EJB
    private ProduktFacade produktFacade;

    public List<ProduktDTO> loadProduktList(){
        List<Produkt> listProdukt = produktFacade.findAll();
        List<ProduktDTO> listProduktDTO = new ArrayList<>();
        for (Produkt produkt : listProdukt) {
            ProduktDTO produktDTO = new ProduktDTO(produkt.getKurs() , produkt.getSkrot() , produkt.getStan() , produkt.getNazwapelna());
            listProduktDTO.add(produktDTO);
        }
        return listProduktDTO;
        
    }    
    
    public void utworzProdukt(Produkt nowyProdukt){
        produktFacade.create(nowyProdukt);
    }
    
    public List<Produkt> getProduktList(){
        return produktFacade.findAll();
    }
    
    public List<Produkt> getOneProduktList(String wybranyProdukt){
        return produktFacade.findProdukt(wybranyProdukt);
    }
    
}
