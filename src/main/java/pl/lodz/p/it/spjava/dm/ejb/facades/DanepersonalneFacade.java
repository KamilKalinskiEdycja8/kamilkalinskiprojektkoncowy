/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.dm.ejb.facades;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.lodz.p.it.spjava.dm.model.Danepersonalne;

/**
 *
 * @author java
 */
@Stateless
public class DanepersonalneFacade extends AbstractFacade<Danepersonalne> {

    @PersistenceContext(unitName = "Kamil_Kalinski_edycja_JEE8_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DanepersonalneFacade() {
        super(Danepersonalne.class);
    }
    
}
